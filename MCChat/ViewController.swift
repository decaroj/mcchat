//
//  ViewController.swift
//  MCChat
//
//  Created by Daniel Cardona Rojas on 9/25/19.
//  Copyright © 2019 Daniel Cardona Rojas. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController {
    @IBOutlet weak var chatTextView: UITextView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var connectionButton: UIButton!

    var isHosting = false

    lazy var peerID: MCPeerID = {
        return MCPeerID(displayName: UIDevice.current.name)

    }()

    lazy var mcSession: MCSession = {
        let session = MCSession(peer: self.peerID, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        return session
    }()

    lazy var disconnectActionSheet: UIAlertController = {
        let alert = UIAlertController(title: "Waiting...", message: "Waiting for other to join", preferredStyle: UIAlertController.Style.actionSheet)

        alert.addAction(UIAlertAction(title: "Disconnect", style: .destructive, handler: { _ in
            self.mcSession.disconnect()
            self.isHosting = false
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        return alert

    }()

    lazy var connectionActionSheet: UIAlertController = {
        let alert = UIAlertController(title: "MCChat", message: "Host or join chat?", preferredStyle: UIAlertController.Style.actionSheet)

        let hostAction = UIAlertAction(title: "Host", style: UIAlertAction.Style.default, handler: { action in
            self.advertiserAssistant = MCAdvertiserAssistant(serviceType: "doesnt-matter", discoveryInfo: nil, session: self.mcSession)
            self.advertiserAssistant?.start()
            self.isHosting = true

        })

        let joinAction = UIAlertAction(title: "Join", style: UIAlertAction.Style.default, handler: { action in
            let mcBrowser = MCBrowserViewController(serviceType: "doesnt-matter", session: self.mcSession)
            mcBrowser.delegate = self
            self.present(mcBrowser, animated: true, completion: nil)

        })

        alert.addAction(hostAction)
        alert.addAction(joinAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        return alert

    }()

    var advertiserAssistant: MCAdvertiserAssistant?

    override func viewDidLoad() {
        super.viewDidLoad()
        sendButton.isEnabled = false
        inputTextField.isEnabled = true
        hideKeyboardWhenTappedAround()
        mcSession.disconnect()
    }

    @IBAction func sendButtonTapped(_ sender: UIButton) {
        guard let text = inputTextField.text, !text.isEmpty else {
            return
        }

        let sendMsg = "\n\(peerID.displayName): \(text)\n"

        let messageData = sendMsg.data(using: .utf8, allowLossyConversion: false)!
        sendMessageToPeers(messageData)
        chatTextView.text += "\nMe: \(text)\n"
        inputTextField.text = nil
    }

    private func sendMessageToPeers(_ data: Data) {
        do {
            try mcSession.send(data, toPeers: mcSession.connectedPeers, with: .reliable)
        } catch let error {
            print("Error sending message: \(error)")
        }

    }

    @IBAction func connectionButtonTapped(_ sender: UIButton) {
        if mcSession.connectedPeers.isEmpty && !isHosting {
            present(connectionActionSheet, animated: true, completion: nil)
        } else if mcSession.connectedPeers.isEmpty && isHosting {
            present(disconnectActionSheet, animated: true, completion: nil)
        } else {

        }

    }
}

// MARK: - MCSessionDelegate
extension ViewController: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print("MCSessionDelegate => \(peerID.displayName) \(state)")
        DispatchQueue.main.async {
            self.sendButton.isEnabled = !session.connectedPeers.isEmpty
        }
    }

    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async {
             if let message = String(data: data, encoding: .utf8) {
                self.chatTextView.text += message
            }
        }
    }

    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }

    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }

    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }


}

// MARK: - MCBrowserViewControllerDelegate
extension ViewController: MCBrowserViewControllerDelegate {
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }

    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
